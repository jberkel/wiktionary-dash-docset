#!/usr/bin/env python
# coding:utf-8

from bs4 import BeautifulSoup, Comment
from shutil import copy
import urllib
from urllib.error import URLError
from urllib.request import urlopen
from urllib.parse import quote
from http.client import HTTPException
import sys
import csv
import os
import re
import sqlite3
from hashlib import md5

DOCSET           = 'Wiktionary.docset'
DOCSET_CONTENTS  = os.path.join(DOCSET, 'Contents')
DOCSET_RESOURCES = os.path.join(DOCSET_CONTENTS, 'Resources')
DOCSET_DOCUMENTS = os.path.join(DOCSET_RESOURCES, 'Documents')
DOCSET_DB        = os.path.join(DOCSET_RESOURCES, 'docSet.dsidx')
CACHE_DIR        = 'cache'
CSS              = ['vector.css', 'docset.css']
PLIST            = 'Info.plist'
ICONS            = ['icon.png', 'icon@2x.png']
BASE_URL         = 'https://en.wiktionary.org'
BASE_WIKI_URL    = BASE_URL + '/wiki/'
DOCUMENTATION_SUFFIX = '/documentation'
NAMESPACE_RE     = re.compile('([^:]+):')
INTERNAL_LINK    = re.compile(r'^/wiki/((?:Template|Module):.+$)')
STRIP = ['#navigation', '#footer', 'script', '#mw-navigation', '.maintenance-box',
         '.mw-editsection', '.mw-jump', '#siteSub', '.subpages', '.printfooter', '#catlinks',
         '.noprint', 'link', '#toc', '.plainlinks']
INCLUDE_SOURCECODE = True


def escape_name(name):
    return name.replace('/', '_').replace(':', '_')


def url_from_name(name, include_source=INCLUDE_SOURCECODE):
    url = BASE_WIKI_URL + quote(name, safe=':/')
    if include_source and name.startswith('Module:'):
        return url
    elif not url.endswith(DOCUMENTATION_SUFFIX):
        return url + DOCUMENTATION_SUFFIX
    else:
        return url


def format_for_dash(url, page_name):
    def delete_comments(soup):
        for element in soup(text=lambda text: isinstance(text, Comment)):
            element.extract()

    def strip(soup):
        map(lambda list: map(lambda e: e.extract(), list), [soup.select(selector) for selector in STRIP])

    def fix_title(soup, new_title):
        for title in soup.select('title') + soup.select('h1#firstHeading span'):
            title.string.replace_with(new_title)

    def fix_links(soup):
        for link in filter(lambda a: 'href' in a.attrs, soup.select('a')):
            href = link['href']
            match = INTERNAL_LINK.match(href)
            fixed_href = None
            if match:
                target = match.group(1)
                fixed_href = escape_name(target) + '.html'
            elif href.startswith('//'):
                fixed_href = 'http:' + href
            elif href.startswith('/'):
                fixed_href = BASE_URL + href

            if fixed_href:
                # print "fixing href %s => %s" % (href, fixed_href)
                link['href'] = fixed_href

    def add_license(soup):
        soup.select('body')[0].append(BeautifulSoup("""
          <div class="license">
            <span>© <a href="https://en.wiktionary.org/">Wiktionary</a>, content available
            under <a href="https://creativecommons.org/licenses/by-sa/2.0/">
                Creative Commons Attribution-ShareAlike License</a>
            </span>
          </div>"""))


    def has_content(soup):
        body_content = soup.select('#bodyContent')
        return body_content and len(body_content[0].get_text().strip()) > 0 or \
            len(soup.select('pre.source-lua')) > 0

    def add_stylesheet(soup, new_stylesheet):
        stylesheet = soup.new_tag('link', rel='stylesheet', href=new_stylesheet, type='text/css')
        soup.select('head')[0].append(stylesheet)

    def add_dash_toc(soup):
        def make_anchor(text, dash_type):
            a = soup.new_tag('a')
            try:
                anchor_text = urllib.quote(text)
            except:
                anchor_text = text
            a['name'] = '//apple_ref/cpp/%s/%s' % (dash_type, anchor_text.replace('/', '%2F'))
            a['class'] = 'dashAnchor'
            return a
        for source in soup.select('pre.source-lua'):
            source.insert_before(BeautifulSoup('<h3><span class="mw-headline">Source</span></h3>'))
            source.insert_before(make_anchor('Source', 'Guide'))
        for headline in soup.select('span.mw-headline'):
            headline.insert_after(make_anchor(headline.get_text(), 'Guide'))

    def fetch_page(url):
        filename = md5(url.encode('utf-8')).hexdigest()
        filepath = os.path.join(CACHE_DIR, filename)
        if os.path.exists(filepath):
            return open(filepath).read()
        try:
            print("fetch_page(%s)" % url)
            response = urlopen(url)
            content = response.read().decode('utf-8')
            with open(filepath, 'w') as f:
                f.write(content)
            return content
        except URLError as e:
            print("error fetching %s: %s" % (url, e))
            return None
        except HTTPException as e:
            print("error fetching %s: %s" % (url, e))
            return None

    def is_redirect(soup):
        content_sub = soup.select('#contentSub')
        return len(content_sub) > 0 and 'Redirected from' in content_sub[0].get_text()

    content = fetch_page(url)
    if content:
        soup = BeautifulSoup(content)
        strip(soup)
        if not is_redirect(soup) and has_content(soup):
            delete_comments(soup)
            fix_title(soup, page_name)
            fix_links(soup)
            for css in CSS:
                add_stylesheet(soup, os.path.basename(css))
            add_dash_toc(soup)
            add_license(soup)
            return soup
        else:
            print("no content or redirect in '%s', skipping" % url)
            return None
    else:
        print("skipping '%s'" % url)


def write_page_to_file(page, content, base_dir):
    escaped_file = escape_name(page) + '.html'
    dest_file = os.path.join(base_dir, escaped_file)
    with open(dest_file, 'w') as f:
        f.write(str(content))
    return os.path.relpath(dest_file, base_dir)


def make_docset_structure():
    if not os.path.exists(DOCSET_DOCUMENTS):
        os.makedirs(DOCSET_DOCUMENTS)
        for css in CSS:
            copy(css, DOCSET_DOCUMENTS)
        copy(PLIST, DOCSET_CONTENTS)
        for icon in ICONS:
            copy(icon, DOCSET)
        create_index()


def create_index():
    with sqlite3.connect(DOCSET_DB) as db:
        cur = db.cursor()
        cur.execute('CREATE TABLE searchIndex(id INTEGER PRIMARY KEY, name TEXT, type TEXT, path TEXT)')
        cur.execute('CREATE UNIQUE INDEX anchor ON searchIndex (name, type, path)')


def add_term_to_index(term, path, type='Module'):
    with sqlite3.connect(DOCSET_DB) as db:
        db.text_factory = str
        cur = db.cursor()
        cur.execute('INSERT OR IGNORE INTO searchIndex(name, type, path) VALUES (?, ?, ?)',
                    (term, type, path))


def create_entry_for_page(name):
    def dash_type_from_name(name):
        m = NAMESPACE_RE.match(name)
        if m:
            namespace = m.group(1)
            if namespace == 'Template':
                return 'Tag'
            elif namespace == 'Module':
                return 'Module'
            else:
                return namespace
        else:
            return ''

    content = format_for_dash(url_from_name(name), name)
    if content:
        file_path = write_page_to_file(name, content, base_dir=DOCSET_DOCUMENTS)
        add_term_to_index(name, file_path, type=dash_type_from_name(name))


def create_from_file(f, name_filter):
    with open(f, 'r') as csvfile:
        names = (title for row in csv.reader(csvfile) if (name_filter(title := row[0])))
        for name in names:
            create_entry_for_page(name)


if __name__ == "__main__":
    if len(sys.argv) > 1 and os.path.exists(sys.argv[1]):
        make_docset_structure()
        if not os.path.exists(CACHE_DIR):
            os.makedirs(CACHE_DIR)
        create_from_file(sys.argv[1], lambda name: "testcase" not in name)
    else:
        print("%s <filename>" % sys.argv[0])
        sys.exit(1)
