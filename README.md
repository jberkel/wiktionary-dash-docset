# Wiktionary docset generator

Small script to generate a [Wiktionary][] docset (modules + templates) for use with either
[Dash][] (OSX) or [Zeal][] (Windows, Linux).

As input it takes a file with a list of templates or modules and produces a `.docset` directory
in the current directory.

    $ pip install --user beautifulsoup4
    $ ./mwdocset.py modules.txt
    $ ./mwdocset.py templates.txt
    # (or just type 'make')

See '[Generating Dash Docsets][]' for more information.

The generator code could probably be adapted to work with other types of MediaWiki installations
as well.

At the moment it generates the documentation by making requests to the MediaWiki server which is far from
ideal but all other alternatives I tried were just too complicated and did not work properly. However there is
a local cache of HTML documents so if you rerun the script no new requests should be made.

# Downloads

Prebuilt docsets are available under [downloads][].

# In action

![recording.gif](https://bitbucket.org/repo/ApxgBB/images/1620083531-Untitled.gif)

# License

This code is licensed under [GPLv2][], the resulting docset / content is licensed under [CC-BY-SA][].

[Wiktionary]: https://en.wiktionary.org/
[Dash]: http://kapeli.com/dash
[Zeal]: http://zealdocs.org/
[Generating Dash Docsets]: http://kapeli.com/docsets
[GPLv2]: https://en.wikipedia.org/wiki/GNU_General_Public_License#Version_2
[CC-BY-SA]: https://en.wiktionary.org/wiki/Wiktionary:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License
[downloads]: https://bitbucket.org/jberkel/wiktionary-dash-docset/downloads