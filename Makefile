NAME=Wiktionary
DOCSET=$(NAME).docset

default: $(DOCSET).tgz
distrib: $(DOCSET).tgz
	cp $(DOCSET).tgz  $(NAME)-$(shell date "+%Y%m%d").docset.tgz

$(DOCSET):
	./mwdocset.py templates.txt
	./mwdocset.py modules.txt

$(DOCSET).tgz: $(DOCSET)
	tar --exclude='.DS_Store' -cvzf $(DOCSET).tgz $(DOCSET)

clean:
	rm -rf $(DOCSET)
	rm -f $(DOCSET).tgz

distclean: clean
	rm -rf cache
